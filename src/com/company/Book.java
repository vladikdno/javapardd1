package com.company;

public class Book {
    public String title;
    public String author;
    public String author2;
    public String author3;
    public int publishYear;
    public String publisher;
    public String[] authors;

    public Book() {     //создаем класс без каких либо данных чтобы в генераторе не агрилось и не выдавало ошибку через selectNOne
    }


    public Book(String title, String author, String author2, int publishYear, String publisher) {
        this.title = title;
        this.author = author;
        this.author2 = author2;
        this.publishYear = publishYear;
        this.publisher = publisher;
    }

    public Book(String title, String author, String author2, String author3, int publishYear, String publisher) {
        this.title = title;
        this.author = author;
        this.author2 = author2;
        this.author3 = author3;
        this.publishYear = publishYear;
        this.publisher = publisher;
    }

    public Book(String title, String[] authors, int publishYear, String publisher) {
        this.title = title;
        this.author = author;
        this.author2 = author2;
        this.author3 = author3;
        this.publishYear = publishYear;
        this.publisher = publisher;
    }


    public boolean isOlderThen(Book book) {
        return publishYear > book.publishYear;
    }

    public boolean isPublishAfter(int year) {
        return publishYear > year;
    }

    public void print() {
        System.out.println(String.format("Название книги: %s; Автор книги: %s; Год издания: %d", title, author, publishYear));
    }

    public void Authors()   {
        String[] authors = new String[]{author, author2, author3};
    }
}
