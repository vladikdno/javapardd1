package com.company;

import java.util.Scanner;

public class BookShelfPolka {
    Book[] books = new Generator().generateBooks();

    public void findAndPrintOldestBook() {
        Book oldestBook = books[0]; // в искомой переменной записали здесь самую старую книгу. а ниже мы сравниваем ее со всеми остальными

        for (int i = 0; i < books.length; i++) {
            if (oldestBook.isOlderThen(books[i])) {       // (oldestBook > books[i].publishYear)
                oldestBook = books[i];
            }
        }
        System.out.println("Автор самой старой книги: " + oldestBook.author + "(" + oldestBook.publishYear + ")");
    }

    /*public void findAndPrintOldestBookByInt() {
        int oldestBookIndex = 0; // Мы берем за основу самую первую книгу как точку оттталкивания для сравнения

        for (int i = 0; i < books.length; i++) { // В цикле фор мы перебираем каждый индекс с массива books для сравнения
            Book oldestBook = books[oldestBookIndex]; //мы оставляем oldestBook в массиве ждя того, чтобы мы сравнивади его с остальными циклами,
            // если она у нас будет перед циклом, то итерация произойдет один раз и на этом все закончитсья
            // ПРо сам цикл. Мы взяли книгу с названием oldestBook и присволили ей значение с массива books, с индексом который у нас в oldestBooksIndex
            Book bookToCompareFromCycle = books[i]; //книга для сравнения с цикла и присвоили значение с массива books по индексу который мы прогнали через цикл

            if (oldestBook.publishYear > bookToCompareFromCycle.publishYear) {// если год самой старой книги больше чем той книги которая пришла из
                //цила, то мы сохраняем ее ниже
                oldestBookIndex = i;
            }
        }

        Book oldestBook = books[oldestBookIndex];
        System.out.println("Автор самой старой книги: " + oldestBook.author + "(" + oldestBook.publishYear + ")");
    }

    public void findAndPrintOldestBookByIntReturn() {

        int oldestBookReturnInt = 0;


        for (int i = books.length - 1; i >= 0; i--) {
            Book oldestBook = books[oldestBookReturnInt];
            Book maybeNewOldestBook = books[i];
            if (oldestBook.publishYear > maybeNewOldestBook.publishYear) {
                oldestBookReturnInt = i;
            }
        }
        Book finalyOldestBook = books[oldestBookReturnInt];
        System.out.println(String.format("Автор самой старой книги: %s, %d", finalyOldestBook.author, finalyOldestBook.publishYear));
    }
*/

    public void findAndPrintBooksNewerThan(int year) {
        for (Book book : books) {
            if (book.isPublishAfter(year)) {
                book.print();
            }
        }
    }

    public void findAuthor() {
        Scanner in = new Scanner(System.in);
        String nameauthor = in.nextLine();
        for (int i = 0; i < books.length; i++) {
            if (books[i].author.equals(nameauthor) == true) {
                System.out.println(books[i].title);
            }
        }
    }

    public void findAuthorAndHisBooks() {
        Scanner authorAndBook = new Scanner(System.in);
        String authorBook = authorAndBook.nextLine();
        for (int i = 0; i < books.length; i++) {
            if (books[i].author2.equals(authorBook) == true) {
                System.out.println(books[2].title + ", " + books[2].publishYear + ", " + books[2].publisher + ". Second author - " + books[2].author);
                System.out.println(books[3].title + ", " + books[3].publishYear + ", " + books[3].publisher + ". Second author - " + books[3].author +
                        ". Third author - " + books[3].author2);
            }
        }
    }

//    public void findBookWithThreeAutors()   {
//        for (int i = 0; i < books.length; i++) {
//            if ()
//        }
//    }
}
